
d3.json("twCounty2010.topo.json", function (data) {
// load data with topojson.js
topo = topojson.feature(data, data.objects["twCounty2010.geo"]);
 
// prepare a Mercator projection for Taiwan
prj = d3.geo.mercator().center([120.979531, 23.978567]).scale(50000);
path = d3.geo.path().projection(prj);
 
// render them on a svg element with id "map"
blocks = d3.select("svg#map").selectAll("path").data(topo.features).enter()
.append("path").attr("d",path);
});