Array.prototype.inArray = function(comparer) { 
  for(var i=0; i < this.length; i++) { 
      if(comparer(this[i])) return true; 
  }
  return false; 
}; 

Array.prototype.pushIfNotExist = function(element, comparer) { 
  if (!this.inArray(comparer)) {
      this.push(element);
  }
}; 

Array.prototype.getIndexBy = function (name, value) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][name] == value) {
            return i;
        }
    }
}

Array.prototype.clone = function() {
	return this.slice(0);
};

var width = 1000,
  	height = 500,
  	theyear = 1970;

var svg = d3.select('#map').append('svg')
          .attr("width", "100%")
          .attr("height", "100%")
          .attr("viewBox", "0 0 960 500")
          .attr("preserveAspectRatio", "xMinYMin meet");

var projection = d3.geo.albersUsa().scale(1000).translate([width / 2, height / 2]);

var path = d3.geo.path().projection(projection);
var offense = [ "T" , "QB" , "WR" , "TE" , "G" , "C" , "RB" , "FB" , "OT" , "OG" ];
var opos = ["OL", "QB", "WR", "RB", "TE"];
var opostotal = [0,0,0,0,0]
var defense = [  "DE" , "OLB" , "CB" , "DT" , "ILB" , "FS" , "SS" , "LB" , "DB" , "LS" , "MLB" , "NT" , "SAF" ];
var dpos = ["LB", "DL", "DB"];
var dpostotal = [0,0,0];
var teamsLen = [];
var teamYears = [];

var drnds = [{ "year" : 1970, "rounds" : 17 },{ "year" : 1971, "rounds" : 17 },{ "year" : 1972, "rounds" : 17 },{ "year" : 1973, "rounds" : 17 },{ "year" : 1974, "rounds" : 17 },{ "year" : 1975, "rounds" : 17 },{ "year" : 1976, "rounds" : 17 },{ "year" : 1977, "rounds" : 12 },{ "year" : 1978, "rounds" : 12 },{ "year" : 1979, "rounds" : 12 },{ "year" : 1980, "rounds" : 12 },{ "year" : 1981, "rounds" : 12 },{ "year" : 1982, "rounds" : 12 },{ "year" : 1983, "rounds" : 12 },{ "year" : 1984, "rounds" : 12 },{ "year" : 1985, "rounds" : 12 },{ "year" : 1986, "rounds" : 12 },{ "year" : 1987, "rounds" : 12 },{ "year" : 1988, "rounds" : 12 },{ "year" : 1989, "rounds" : 12 },{ "year" : 1990, "rounds" : 12 },{ "year" : 1991, "rounds" : 12 },{ "year" : 1992, "rounds" : 12 },{ "year" : 1993, "rounds" : 8 },{ "year" : 1994, "rounds" : 7 },{ "year" : 1995, "rounds" : 7 },{ "year" : 1996, "rounds" : 7 },{ "year" : 1997, "rounds" : 7 },{ "year" : 1998, "rounds" : 7 },{ "year" : 1999, "rounds" : 7 },{ "year" : 2000, "rounds" : 7 },{ "year" : 2001, "rounds" : 7 },{ "year" : 2002, "rounds" : 7 },{ "year" : 2003, "rounds" : 7 },{ "year" : 2004, "rounds" : 7 },{ "year" : 2005, "rounds" : 7 },{ "year" : 2006, "rounds" : 7 },{ "year" : 2007, "rounds" : 7 },{ "year" : 2008, "rounds" : 7 },{ "year" : 2009, "rounds" : 7 },{ "year" : 2010, "rounds" : 7 },{ "year" : 2011, "rounds" : 7 },{ "year" : 2012, "rounds" : 7 },{ "year" : 2013, "rounds" : 7 },{ "year" : 2014, "rounds" : 7 }];


function outputUpdate(year) {
	d3.select('#year').text(year);
	theyear = year;
}  
outputUpdate(1970);

d3.json('us.json', function(error, us) {

    var data = topojson.feature(us, us.objects.state).features;

	d3.csv("csv/teamstad.csv", function(error, ts) {

		var teams = [];
		var positions = [];
		var year = 0;

		ts.forEach(function(element){
			element.years = [];
		});

		for (var j = 1970; j <= 2014; j++) {
			var now = j;
			ts.forEach(function(element){
				element.years.push({"year":now});
			});
			runall(now);		
		};

		function runall(now){

			d3.csv('csv/standings/standings' + now + '.csv', function(err, stands) {

				teamsLen.push(stands.length);

				stands.forEach(function(d) {
					if(now === 1970){
						teams.pushIfNotExist(d.Team, function(e){
						  return e === d.Team;
						});						
					}
					ts.forEach(function(e){	
						if(d.Team === e.Team){

							e.years[now - 1970].wins = d.W;
							e.years[now - 1970].losses = d.L;
							e.years[now - 1970].PF = d.PF;
							e.years[now - 1970].PA = d.PA;
							e.years[now - 1970].NP = d["Net Pts"];

							if(now > 1970){
								if((now - 1970) < (e.years.length  - 1)){
									e.years[now - 1970].jump = d.W - (e.years[now - 1971].wins);
								}else{
									e.jump = 0;
								}								
							}
						}
					});
				});

				if(now === 2014){
					drawAll(ts);
				}	
			});	
			d3.csv('csv/rushing/stats' + now + '.csv', function(err, stands) {
				stands.forEach(function(d) {
					ts.forEach(function(e){	
						if(d.Team === e.Team){
							e.years[now - 1970].rushrank = d.Rk;
						}
					});
				});
			});
			d3.csv('csv/passing/stats' + now + '.csv', function(err, stands) {
				stands.forEach(function(d){
					ts.forEach(function(e){	
						if(d.Team === e.Team){
							e.years[now - 1970].passrank = d.Rk;
							var a = ((parseInt(d.Comp) / parseInt(d.Att)) - 0.3) * 5;
							var b = ((parseInt(d.Yds) / parseInt(d.Att)) - 3) * 0.25
							var c = (parseInt(d.TD) / parseInt(d.Att)) * 20;
							var z = 2.375 - ((parseInt(d.Int) / parseInt(d.Att)) * 25)
							e.years[now - 1970].QBR = ((mm(a) + mm(b) + mm(c) + mm(z)) / 6) * 100;

							function mm(en){
								if( en < 0 ){
									return 0;
								}else if( en > 2.375 ){
									return 2.375;
								}else{
									return en;
								}
							}
						}
					});
				});
			});
			d3.csv('csv/defense/stats' + now + '.csv', function(err, stands) {
				stands.forEach(function(d) {
					ts.forEach(function(e){	
						if(d.Team === e.Team){
							e.years[now - 1970].defrank = d.Rk;
							e.years[now - 1970].dpwrrank = (d["Pts/G"] * d["Yds/G"]) / 20;
						}
					});
				});
			});
			d3.csv('csv/drafts/draft' + now + '.csv', function(err, draft) {

				ts.forEach(function(g){
					g.years[now - 1970].off = 0;
					g.years[now - 1970].def = 0;
					g.years[now - 1970].st = 0;

					g.years[now - 1970].offtot = 0;
					g.years[now - 1970].deftot = 0;
					g.years[now - 1970].sttot = 0;

					g.years[now - 1970].wdp = 0;
					g.years[now - 1970].wdptot = 0;

					g.years[now - 1970].qbt = 0;
					g.years[now - 1970].qbv = 0;				
				});

				draft.forEach(function(d) {
					ts.forEach(function(e){	
						if(d.TEAM === e.Team){
							if(e.years[now - 1970][d.POSITION]){
								e.years[now - 1970][d.POSITION + "v"] += (1 / Math.ceil(d.SEL / 30));
								e.years[now - 1970][d.POSITION + "t"] += 1;
							}else{
								e.years[now - 1970][d.POSITION + "v"] = (1 / Math.ceil(d.SEL / 30));
								e.years[now - 1970][d.POSITION + "t"] = 1;
							}
							e.years[now - 1970].wdp += (1 / Math.ceil(d.SEL / 30));
							e.years[now - 1970].wdptot += 1;

							if(offense.indexOf(d.POSITION) > -1){
								//give weighted round selection
								e.years[now - 1970].off += (1 / Math.ceil(d.SEL / 30));
								e.years[now - 1970].offtot += 1;
							}else if(defense.indexOf(d.POSITION) > -1){
								e.years[now - 1970].def += (1 / Math.ceil(d.SEL / 30));
								e.years[now - 1970].deftot += 1;
							}else{
								e.years[now - 1970].st += (1 / Math.ceil(d.SEL / 30));
								e.years[now - 1970].sttot += 1;
							}
						}
					});
				});

				if(now === 2014){
					makeLines(ts);
				}
			});	
		}

		function makeLines(arr){

			for (var i = 1970; i <= 2014; i++) {
				ts.sort(function(a, b) {
					if(typeof a.years[i - 1970].dpwrrank === 'undefined'){
						a.years[i - 1970].dpwrrank = 1000000;
					}
					if(typeof b.years[i - 1970].dpwrrank === 'undefined'){
						b.years[i - 1970].dpwrrank = 1000000;
					}					
				    return a.years[i - 1970].dpwrrank - b.years[i - 1970].dpwrrank;
				});
				ts.forEach(function(element, index){
					element.years[i - 1970].dpr = index + 1;
					element.years[i - 1970].PlayersPR = element.years[i - 1970].wdptot / drnds[i - 1970].rounds;
				});
			};

			var peepee = [];

			ts.forEach(function(element, index){
				for (var i = 1970; i <= 2009; i++) {
					element.years[i - 1970]["fiveyr"] = {};

					element.years[i - 1970]["fiveyr"].RR = getAverage(i, 5, "rushrank", element);
					element.years[i - 1970]["fiveyr"].PR = getAverage(i, 5, "passrank", element);
					element.years[i - 1970]["fiveyr"].DR = getAverage(i, 5, "dpwrrank", element);
					element.years[i - 1970]["fiveyr"].DPR = getAverage(i, 5, "dpr", element);
					element.years[i - 1970]["fiveyr"].WINS = getAverage(i, 5, "wins", element);
					element.years[i - 1970]["fiveyr"].LOSSES = getAverage(i, 5, "losses", element);					
					element.years[i - 1970]["fiveyr"].PDPR = getAverage(i, 5, "PlayersPR", element);					

					if(element.years[i - 1970]["offtot"] !== 0 && element.years[i - 1970]["deftot"] !== 0){
						element.years[i - 1970].DDAV = element.years[i - 1970]["def"] / element.years[i - 1970]["deftot"];
						element.years[i - 1970].ODAV = element.years[i - 1970]["off"]/ element.years[i - 1970]["offtot"];						
					}else{
						element.years[i - 1970]["fiveyr"].DDAV = 0;
						element.years[i - 1970]["fiveyr"].ODAV = 0;												
					}
				}
				for (var i = 2010; i <= 2014; i++) {
					element.years[i - 1970]["fiveyr"] = {};
					element.years[i - 1970]["fiveyr"].RR = -1;
					element.years[i - 1970]["fiveyr"].PR = -1;
					element.years[i - 1970]["fiveyr"].DR = -1;
					element.years[i - 1970]["fiveyr"].DPR = -1;
					element.years[i - 1970]["fiveyr"].WINS = -1;
					element.years[i - 1970]["fiveyr"].LOSSES = -1;					
					element.years[i - 1970]["fiveyr"].PDPR = -1;	
				};
			});
			function getAverage( year, spans, prop, element ){
				var eval = 0;
				var modspan = spans;
				for (var i = year; i < year + spans; i++) {
					if( element.years[i - 1970][prop] > -1 ){
						eval += parseFloat(element.years[i - 1970][prop]);
					}else{
						modspan -= 1;
					}
				};
				if(modspan > 0){
					return eval / modspan;
				}else{
					return 0;
				}
			}	
			function maxEachYear( arrz, valuez, storez ){
				for (var i = 2014; i >= 1970; i--) {

					var valz = d3.max(arrz, function(m) { return m[valuez+i];} );
					var teamz;
					ts.forEach(function(p){
						if(p[valuez+i] === valz){
							teamz = p.Team;
						}
					});
					storez.push({
						"year" : i,
						"QBR" : valz,
						"team" : teamz
					});
				};
			}
			var cock = d3.max(peepee, function(m) { return m.QBR;} );
			var penis, ballz;
			peepee.forEach(function(p){
				if(p["QBR"] === cock){
					penis = p.team;
					ballz = p.year;
				}
			});		

			arr.forEach(function(d){
				var tys = {};
				var bodz = d3.select('#lines').append('section')
							.attr('class', function(){return d.Team;});

				var liner = bodz.append('svg')
				            .attr("width", "100%")
				            .attr("height", "100%")
				            .attr("viewBox", "0 0 960 500")
				            .attr("preserveAspectRatio", "xMinYMin meet");	

				bodz.append('h2').text(function(){return d.Team;});

				var offs = [];	
				var defs = [];	
				var sts = [];

				var passes = [];
				var rushes = [];
				var defenses = [];

				for (var i = 1970; i <= 2014; i++) {

					var df = {};
					var o = {};
					var s = {};
					var ds = {};
					var rs = {};
					var ps = {};

					df.tval = d.years[i - 1970]["def"];
					o.tval = d.years[i - 1970]["off"];
					s.tval = d.years[i - 1970]["st"];

					df.yr = i;
					o.yr = i;
					s.yr = i;

					ds.tval = d.years[i - 1970]["dpr"];
					ps.tval = d.years[i - 1970]["passrank"];
					rs.tval = d.years[i - 1970]["rushrank"];

					ds.yr = i;
					ps.yr = i;
					rs.yr = i;

					offs.push(o);
					defs.push(df);
					sts.push(s);

					defenses.push(ds);
					passes.push(ps);
					rushes.push(rs);

				};		

				var xScaleTot = d3.scale.linear().domain([0,offs.length - 1]).range([0,960]);
			    var minOff = d3.min(offs, function(m) { return m.tval;} );
			    var maxOff = d3.max(offs, function(m) { return m.tval;} );
			    var minDef = d3.min(defs, function(m) { return m.tval;} );
			    var maxDef = d3.max(defs, function(m) { return m.tval;} );
			    var tots = [minOff , maxOff , minDef , maxDef];


			    var minOffs = d3.min(offs, function(m) { 
			    	if( m.tval === 0 ){
			    		return 10000000000;
			    	}else{
				    	return m.tval;
			    	}
			    } );
			    var minDefs = d3.min(defs, function(m) { 
			    	if( m.tval === 0 ){
			    		return 10000000000;
			    	}else{
				    	return m.tval;
			    	}
			    } );

			    tys.Team = d.Team;
			    tys.maxO = maxOff;
			    tys.maxD = maxDef;
			    tys.minO = minOffs;
			    tys.minD = minDefs;

			    tys.maxOY = tough(offs, tys.maxO);
			    tys.maxDY = tough(defs, tys.maxD);
			    tys.minOY = tough(offs, tys.minO);
			    tys.minDY = tough(defs, tys.minD);			    

			    tys.minPR = d3.max(passes, function(m) { return parseInt(m.tval);} );
			    tys.minRR = d3.max(rushes, function(m) { return parseInt(m.tval);} );
			    tys.minDR = d3.max(defenses, function(m) { return parseInt(m.tval);} );
			    tys.maxPR = d3.min(passes, function(m) { return parseInt(m.tval);} );
			    tys.maxRR = d3.min(rushes, function(m) { return parseInt(m.tval);} );
			    tys.maxDR = d3.min(defenses, function(m) { return parseInt(m.tval);} );		

			    tys.minPRY = tough(passes, tys.minPR);
			    tys.minRRY = tough(rushes, tys.minRR);
			    tys.minDRY = tough(defenses, tys.minDR);
			    tys.maxPRY = tough(passes, tys.maxPR);
			    tys.maxRRY = tough(rushes, tys.maxRR);
			    tys.maxDRY = tough(defenses, tys.maxDR);		

			    teamYears.push(tys);

			    function tough(arr, comp){
			    	var users = [];
			    	arr.forEach(function(el){
			    		if(parseInt(el.tval) === parseInt(comp)){
			    			users.push(el.yr);
			    		}
			    	});
			    	return users;
			    }

			    var maxTot = d3.max(tots, function(m) { return m;} );
			   
			    var yScaleTot = d3.scale.linear().domain([0,(maxTot)]).range([500, 0]);
			    var opac = d3.scale.linear().domain([0, maxTot]).range([0, 1]);

			    //build x and y axes
			    var ticks = [0,maxTot/2,maxTot];

			    var yAxisTot = d3.svg.axis().scale(yScaleTot)
			            .orient("left")
			            .tickValues(ticks)
			        	.tickSize(-width);

			    var xAxisTot = d3.svg.axis().scale(xScaleTot).ticks(44).orient("bottom").tickSize(-500,0,0).tickFormat("");

			    //put x and y axes on page
			    var xAxisGroupTot = liner.append("g")
			            .attr("class", "x axis grid")
			            .attr("transform", "translate(0,500)")
			            .call(xAxisTot);
			    var yAxisGroupTot = liner.append("g")
			            .attr("class", "y axis grid")
			            .call(yAxisTot);
			    yAxisGroupTot.selectAll("text").style("opacity", "0");
			    yAxisGroupTot.selectAll(".domain").style("opacity", "0");

				// var funk = liner.append('g').attr('class', 'line-group');
				// var funklines = funk.selectAll('line').data(offs).enter().append('line');		 
				// var funkcircs = funk.selectAll('circle').data(offs).enter().append('circle');

				var splash = liner.append('g').attr('class', 'line-group');
				var splashlines = splash.selectAll('line').data(defs).enter().append('line');		 
				var splashcircs = splash.selectAll('circle').data(defs).enter().append('circle');		 

			    var dg = liner.append('g').attr('class', 'line-group');
			    var dglines = dg.selectAll('line').data(defenses).enter().append('line');
			    var dgcircs = dg.selectAll('circle').data(defenses).enter().append('circle');

				// var rg = liner.append('g').attr('class', 'line-group');
				// var rglines = rg.selectAll('line').data(rushes).enter().append('line');		
				// var rgcircs = rg.selectAll('circle').data(rushes).enter().append('circle');		

				// var pg = liner.append('g').attr('class', 'line-group');
				// var pglines = pg.selectAll('line').data(passes).enter().append('line');		  
				// var pgcircs = pg.selectAll('circle').data(passes).enter().append('circle');		  


			    splashlines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			            return yScaleTot(m.tval);
			          })
			          .attr('y2', function(m,i){
			            if(i < offs.length - 1){
			              return yScaleTot(defs[i+1].tval);
			            }else{
			              return yScaleTot(m.tval);  
			            }
			          })
			          .attr('stroke', '#0004FF')
			          .attr('stoke-width', 2);
				splashcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				            return yScaleTot(m.tval);           
				          })
				          .attr('r', 4)
				          .attr('fill', '#0004FF')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);		

			    dglines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			          	var pv;
			          	var tl = teamsLen[i];

			          	if(m.tval){
			          		pv = m.tval;
			          	}else{
			          		pv = teamsLen[i] + 1;
			          	}

			          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why1);
			          })
			          .attr('y2', function(m,i){
			          	var j;
			          	if(i === defenses.length-1){
			          		j = i;
			          	}else{
			          		j = i + 1;
			          	}
			          	var pv;
			          	var tl = teamsLen[j];

			          	if(defenses[j].tval){
			          		pv = defenses[j].tval;
			          	}else{
			          		pv = teamsLen[j] + 1;
			          	}

			          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why2);
			          })
			          .attr('stroke', '#00FF22')
			          .attr('stoke-width', 2);
				dgcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				          	var pv;
				          	var tl = teamsLen[i];

				          	if(m.tval){
				          		pv = m.tval;
				          	}else{
				          		pv = teamsLen[i] + 1;
				          	}

				          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

				            return  yScaleTot(why1);          
				          })
				          .attr('r', 4)
				          .attr('fill', '#00FF22')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);		

			 //    funklines.attr('x1', function(m,i){
			 //            return xScaleTot(i);
			 //          })
			 //          .attr('x2', function(m,i){
			 //            if(i < offs.length - 1){
			 //              return xScaleTot(i + 1);
			 //            }else{
			 //              return xScaleTot(i);
			 //            }            
			 //          })
			 //          .attr('y1', function(m,i){
			 //            return yScaleTot(m.tval);
			 //          })
			 //          .attr('y2', function(m,i){
			 //            if(i < offs.length - 1){
			 //              return yScaleTot(offs[i+1].tval);
			 //            }else{
			 //              return yScaleTot(m.tval);  
			 //            }
			 //          })
			 //          .attr('stroke', '#FF0000')
			 //          .attr('stoke-width', 2);
				// funkcircs.attr('cx', function(m,i){
				//             return xScaleTot(i);
				//           })
				//           .attr('cy', function(m,i){
				//             return yScaleTot(m.tval);           
				//           })
				//           .attr('r', 4)
				//           .attr('fill', '#FF0000')
				//           .style("opacity", function(m){return opac(m.tval);})
				//           .on('mouseenter', circtip.show)
				//           .on('mouseleave', circtip.hide);		          

			 //    rglines.attr('x1', function(m,i){
			 //            return xScaleTot(i);
			 //          })
			 //          .attr('x2', function(m,i){
			 //            if(i < offs.length - 1){
			 //              return xScaleTot(i + 1);
			 //            }else{
			 //              return xScaleTot(i);
			 //            }            
			 //          })
			 //          .attr('y1', function(m,i){
			 //          	var pv;
			 //          	var tl = teamsLen[i];

			 //          	if(m.tval){
			 //          		pv = m.tval;
			 //          	}else{
			 //          		pv = teamsLen[i] + 1;
			 //          	}

			 //          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			 //            return  yScaleTot(why1);
			 //          })
			 //          .attr('y2', function(m,i){
			 //          	var j;
			 //          	if(i === rushes.length-1){
			 //          		j = i;
			 //          	}else{
			 //          		j = i + 1;
			 //          	}
			 //          	var pv;
			 //          	var tl = teamsLen[j];

			 //          	if(rushes[j].tval){
			 //          		pv = rushes[j].tval;
			 //          	}else{
			 //          		pv = teamsLen[j] + 1;
			 //          	}

			 //          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			 //            return  yScaleTot(why2);
			 //          })
			 //          .attr('stroke', '#FFA200')
			 //          .attr('stoke-width', 2);
				// rgcircs.attr('cx', function(m,i){
				//             return xScaleTot(i);
				//           })
				//           .attr('cy', function(m,i){
				//           	var pv;
				//           	var tl = teamsLen[i];

				//           	if(m.tval){
				//           		pv = m.tval;
				//           	}else{
				//           		pv = teamsLen[i] + 1;
				//           	}

				//           	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

				//             return  yScaleTot(why1);        
				//           })
				//           .attr('r', 4)
				//           .attr('fill', '#FFA200')
				//           .style("opacity", function(m){return opac(m.tval);})
				//           .on('mouseenter', circtip.show)
				//           .on('mouseleave', circtip.hide);	

			 //    pglines.attr('x1', function(m,i){
			 //            return xScaleTot(i);
			 //          })
			 //          .attr('x2', function(m,i){
			 //            if(i < offs.length - 1){
			 //              return xScaleTot(i + 1);
			 //            }else{
			 //              return xScaleTot(i);
			 //            }            
			 //          })
			 //          .attr('y1', function(m,i){
			 //          	var pv;
			 //          	var tl = teamsLen[i];

			 //          	if(m.tval){
			 //          		pv = m.tval;
			 //          	}else{
			 //          		pv = teamsLen[i] + 1;
			 //          	}

			 //          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			 //            return  yScaleTot(why1);
			 //          })
			 //          .attr('y2', function(m,i){
			 //          	var j;
			 //          	if(i === passes.length-1){
			 //          		j = i;
			 //          	}else{
			 //          		j = i + 1;
			 //          	}
			 //          	var pv;
			 //          	var tl = teamsLen[j];

			 //          	if(passes[j].tval){
			 //          		pv = passes[j].tval;
			 //          	}else{
			 //          		pv = teamsLen[j] + 1;
			 //          	}

			 //          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			 //            return  yScaleTot(why2);
			 //          })
			 //          .attr('stroke', '#F6FF00')
			 //          .attr('stoke-width', 2);
				// pgcircs.attr('cx', function(m,i){
				//             return xScaleTot(i);
				//           })
				//           .attr('cy', function(m,i){
				// 	          	var pv;
				// 	          	var tl = teamsLen[i];

				// 	          	if(m.tval){
				// 	          		pv = m.tval;
				// 	          	}else{
				// 	          		pv = teamsLen[i] + 1;
				// 	          	}

				// 	          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

				// 	            return  yScaleTot(why1);     
				//           })
				//           .attr('r', 4)
				//           .attr('fill', '#F6FF00')
				//           .style("opacity", function(m){return opac(m.tval);})
				//           .on('mouseenter', circtip.show)
				//           .on('mouseleave', circtip.hide);	
			});

			var mimi = d3.min(teamYears, function(m) { return m.minO;} );
			var mama = d3.max(teamYears, function(m) { return m.maxO;} );

			teamYears.forEach(function(m){
				if(m.maxO === mama){
					console.log(m.Team);
				}
			});
			d3.select("body").append('p').text(JSON.stringify(ts));
		}

		function drawAll(arr){
			circles = svg.selectAll("circle").data(arr);
			
			circles.enter()
			   .append("circle")
			   .attr("fill", function(d,i){
			   		return d.color1;
			   })
			   .attr("stroke", function(d,i){
			   		return d.color2;
			   })
			   .style("opacity", 0.97)
			   .attr("stroke-width", 5)
			   .on("mouseover", tip.show)
			   .on("mouseleave", tip.hide);

			for (var j = 2014; j >= 1970; j--) {
				var now = j;
				timeCircs(arr, now);		
			};
		}

		function timeCircs(arr, tim){
			var timing = tim - 1970;
			setTimeout(function(){
				d3.select('#year').text(tim);
				$('#years').val(tim);
				drawCircles(arr, tim);
			}, timing * 1000);
		}

		function drawCircles(poop, year){
			theyear = year;

			circles.each(function(d){
				var top =  parseInt(d["wins"+year]);
		   		var btm =  parseInt(d["wins"+year]) + parseInt(d["losses"+year]);
		   		var perc;

		   		if(top === 0){
		   			perc = 0.01;
		   		}else if( top > 0 ){
		   			perc = top / btm;
		   		}else{
		   			perc = 0;
		   		}
				d.r = (5 * perc) * 8;
				d.cx = projection([d.lon, d.lat])[0];
				d.cy = projection([d.lon, d.lat])[1];
			});

			circles
			   .attr("cx", function(d, i) { return d.cx; })
			   .attr("cy", function(d, i) { return d.cy; });

			// prepare data for force layout
			forceNodes = [];
			for(i = 0 ; i < poop.length ; i++ ) {
				forceNodes.push(poop[i]);
			}

			// force layout, makes circles closer.
			// This is useful when circle radius changes.
			force = d3.layout.force().gravity(3.0).size([width,height]).charge(-1).nodes(forceNodes);

			force.on("tick", function() {
				for(i = 0 ; i < poop.length ; i++) {
					for(j = 0 ; j < poop.length ; j++) { 
						it = poop[i]; 
					  	jt = poop[j]; 
						if(i==j) continue; 
						// not collide with self 
						r = it.r + jt.r; 
						// it.c is the centroid of each county and initial position of circle 
						itx = it.cx + 6; 
						ity = it.cy + 6; 
						jtx = jt.cx + 6; 
						jty = jt.cy + 6; 
						// distance between centroid of two circles 
						d = Math.sqrt( (itx - jtx) * (itx - jtx) + (ity - jty) * (ity - jty) ); 
						if(r > d) { 
							// there's a collision if distance is smaller than radius
							dr = ( r - d ) / ( d * 1 );
							it.cx = it.cx + ( itx - jtx ) * dr;
							it.cy = it.cy + ( ity - jty ) * dr;
						}
					}
				}
				circles.transition().duration(250).ease("cubic")
			   	.attr("r", function(d, i){ 
			   		if(d.r < 3.5){
			   			d.r = 1;
			   		}else{
			   			return d.r - 2.5;
			   		}
			   	 })
				.attr("cx",function(it) { return it.cx; })
				.attr("cy",function(it) { return it.cy; });
			});


			force.start();
		}

	    var tip = d3.tip()
	      .attr('class', 'd3-tip')
	      .offset([-10, 0])
	      .html(function(d) {
	      	return '<div>Team : ' + 
	      	d.Team + '</div><div>O-Draft Stock : ' +
	      	Math.round(d["off"+theyear] * 1000) / 1000 + '</div><div>D-Draft Stock : ' +
	      	Math.round(d["def"+theyear] * 1000) / 1000 + '</div><div>Wins : ' +
	      	d["wins"+theyear] + '</div>';
	      });

	    var circtip = d3.tip()
	      .attr('class', 'd3-tip')
	      .offset([-10, 0])
	      .html(function(d) {
	      	return '<div>' + d.yr + '</div><div>' + d.tval + '</div>';
	      });

	    svg.call(tip).call(circtip);       

	    var map = svg.selectAll('.state')
	      .data(data)
	      .enter().append('path')
	      .attr('class', 'state')
	      .attr('d', path)
	      .attr('fill', '#fff')
	      .attr('stroke', '#000')
	      .attr('stroke-width', '1');

	});
});


