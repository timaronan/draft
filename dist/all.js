// d3.tip
// Copyright (c) 2013 Justin Palmer
//
// Tooltips for d3.js SVG visualizations

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module with d3 as a dependency.
    define(['d3'], factory)
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS
    module.exports = function(d3) {
      d3.tip = factory(d3)
      return d3.tip
    }
  } else {
    // Browser global.
    root.d3.tip = factory(root.d3)
  }
}(this, function (d3) {

  // Public - contructs a new tooltip
  //
  // Returns a tip
  return function() {
    var direction = d3_tip_direction,
        offset    = d3_tip_offset,
        html      = d3_tip_html,
        node      = initNode(),
        svg       = null,
        point     = null,
        target    = null

    function tip(vis) {
      svg = getSVGNode(vis)
      point = svg.createSVGPoint()
      document.body.appendChild(node)
    }

    // Public - show the tooltip on the screen
    //
    // Returns a tip
    tip.show = function() {
      var args = Array.prototype.slice.call(arguments)
      if(args[args.length - 1] instanceof SVGElement) target = args.pop()

      var content = html.apply(this, args),
          poffset = offset.apply(this, args),
          dir     = direction.apply(this, args),
          nodel   = d3.select(node),
          i       = directions.length,
          coords,
          scrollTop  = document.documentElement.scrollTop || document.body.scrollTop,
          scrollLeft = document.documentElement.scrollLeft || document.body.scrollLeft

      nodel.html(content)
        .style({ opacity: 1, 'pointer-events': 'all' })

      while(i--) nodel.classed(directions[i], false)
      coords = direction_callbacks.get(dir).apply(this)
      nodel.classed(dir, true).style({
        top: (coords.top +  poffset[0]) + scrollTop + 'px',
        left: (coords.left + poffset[1]) + scrollLeft + 'px'
      })

      return tip
    }

    // Public - hide the tooltip
    //
    // Returns a tip
    tip.hide = function() {
      var nodel = d3.select(node)
      nodel.style({ opacity: 0, 'pointer-events': 'none' })
      return tip
    }

    // Public: Proxy attr calls to the d3 tip container.  Sets or gets attribute value.
    //
    // n - name of the attribute
    // v - value of the attribute
    //
    // Returns tip or attribute value
    tip.attr = function(n, v) {
      if (arguments.length < 2 && typeof n === 'string') {
        return d3.select(node).attr(n)
      } else {
        var args =  Array.prototype.slice.call(arguments)
        d3.selection.prototype.attr.apply(d3.select(node), args)
      }

      return tip
    }

    // Public: Proxy style calls to the d3 tip container.  Sets or gets a style value.
    //
    // n - name of the property
    // v - value of the property
    //
    // Returns tip or style property value
    tip.style = function(n, v) {
      if (arguments.length < 2 && typeof n === 'string') {
        return d3.select(node).style(n)
      } else {
        var args =  Array.prototype.slice.call(arguments)
        d3.selection.prototype.style.apply(d3.select(node), args)
      }

      return tip
    }

    // Public: Set or get the direction of the tooltip
    //
    // v - One of n(north), s(south), e(east), or w(west), nw(northwest),
    //     sw(southwest), ne(northeast) or se(southeast)
    //
    // Returns tip or direction
    tip.direction = function(v) {
      if (!arguments.length) return direction
      direction = v == null ? v : d3.functor(v)

      return tip
    }

    // Public: Sets or gets the offset of the tip
    //
    // v - Array of [x, y] offset
    //
    // Returns offset or
    tip.offset = function(v) {
      if (!arguments.length) return offset
      offset = v == null ? v : d3.functor(v)

      return tip
    }

    // Public: sets or gets the html value of the tooltip
    //
    // v - String value of the tip
    //
    // Returns html value or tip
    tip.html = function(v) {
      if (!arguments.length) return html
      html = v == null ? v : d3.functor(v)

      return tip
    }

    function d3_tip_direction() { return 'n' }
    function d3_tip_offset() { return [0, 0] }
    function d3_tip_html() { return ' ' }

    var direction_callbacks = d3.map({
      n:  direction_n,
      s:  direction_s,
      e:  direction_e,
      w:  direction_w,
      nw: direction_nw,
      ne: direction_ne,
      sw: direction_sw,
      se: direction_se
    }),

    directions = direction_callbacks.keys()

    function direction_n() {
      var bbox = getScreenBBox()
      return {
        top:  bbox.n.y - node.offsetHeight,
        left: bbox.n.x - node.offsetWidth / 2
      }
    }

    function direction_s() {
      var bbox = getScreenBBox()
      return {
        top:  bbox.s.y,
        left: bbox.s.x - node.offsetWidth / 2
      }
    }

    function direction_e() {
      var bbox = getScreenBBox()
      return {
        top:  bbox.e.y - node.offsetHeight / 2,
        left: bbox.e.x
      }
    }

    function direction_w() {
      var bbox = getScreenBBox()
      return {
        top:  bbox.w.y - node.offsetHeight / 2,
        left: bbox.w.x - node.offsetWidth
      }
    }

    function direction_nw() {
      var bbox = getScreenBBox()
      return {
        top:  bbox.nw.y - node.offsetHeight,
        left: bbox.nw.x - node.offsetWidth
      }
    }

    function direction_ne() {
      var bbox = getScreenBBox()
      return {
        top:  bbox.ne.y - node.offsetHeight,
        left: bbox.ne.x
      }
    }

    function direction_sw() {
      var bbox = getScreenBBox()
      return {
        top:  bbox.sw.y,
        left: bbox.sw.x - node.offsetWidth
      }
    }

    function direction_se() {
      var bbox = getScreenBBox()
      return {
        top:  bbox.se.y,
        left: bbox.e.x
      }
    }

    function initNode() {
      var node = d3.select(document.createElement('div'))
      node.style({
        position: 'absolute',
        top: 0,
        opacity: 0,
        'pointer-events': 'none',
        'box-sizing': 'border-box'
      })

      return node.node()
    }

    function getSVGNode(el) {
      el = el.node()
      if(el.tagName.toLowerCase() === 'svg')
        return el

      return el.ownerSVGElement
    }

    // Private - gets the screen coordinates of a shape
    //
    // Given a shape on the screen, will return an SVGPoint for the directions
    // n(north), s(south), e(east), w(west), ne(northeast), se(southeast), nw(northwest),
    // sw(southwest).
    //
    //    +-+-+
    //    |   |
    //    +   +
    //    |   |
    //    +-+-+
    //
    // Returns an Object {n, s, e, w, nw, sw, ne, se}
    function getScreenBBox() {
      var targetel   = target || d3.event.target;

      while ('undefined' === typeof targetel.getScreenCTM && 'undefined' === targetel.parentNode) {
          targetel = targetel.parentNode;
      }

      var bbox       = {},
          matrix     = targetel.getScreenCTM(),
          tbbox      = targetel.getBBox(),
          width      = tbbox.width,
          height     = tbbox.height,
          x          = tbbox.x,
          y          = tbbox.y

      point.x = x
      point.y = y
      bbox.nw = point.matrixTransform(matrix)
      point.x += width
      bbox.ne = point.matrixTransform(matrix)
      point.y += height
      bbox.se = point.matrixTransform(matrix)
      point.x -= width
      bbox.sw = point.matrixTransform(matrix)
      point.y -= height / 2
      bbox.w  = point.matrixTransform(matrix)
      point.x += width
      bbox.e = point.matrixTransform(matrix)
      point.x -= width / 2
      point.y -= height / 2
      bbox.n = point.matrixTransform(matrix)
      point.y += height
      bbox.s = point.matrixTransform(matrix)

      return bbox
    }

    return tip
  };

}));
Array.prototype.inArray = function(comparer) { 
  for(var i=0; i < this.length; i++) { 
      if(comparer(this[i])) return true; 
  }
  return false; 
}; 

Array.prototype.pushIfNotExist = function(element, comparer) { 
  if (!this.inArray(comparer)) {
      this.push(element);
  }
}; 

Array.prototype.getIndexBy = function (name, value) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][name] == value) {
            return i;
        }
    }
}

Array.prototype.clone = function() {
	return this.slice(0);
};

var width = 1000,
  	height = 500,
  	theyear = 1970;

var svg = d3.select('#map').append('svg')
          .attr("width", "100%")
          .attr("height", "100%")
          .attr("viewBox", "0 0 960 500")
          .attr("preserveAspectRatio", "xMinYMin meet");

var projection = d3.geo.albersUsa().scale(1000).translate([width / 2, height / 2]);

var path = d3.geo.path().projection(projection);
var offense = [ "T" , "QB" , "WR" , "TE" , "G" , "C" , "RB" , "FB" , "OT" , "OG" ];
var opos = ["OL", "QB", "WR", "RB", "TE"];
var opostotal = [0,0,0,0,0]
var defense = [  "DE" , "OLB" , "CB" , "DT" , "ILB" , "FS" , "SS" , "LB" , "DB" , "LS" , "MLB" , "NT" , "SAF" ];
var dpos = ["LB", "DL", "DB"];
var dpostotal = [0,0,0];
var teamsLen = [];
var teamYears = [];

function outputUpdate(year) {
	d3.select('#year').text(year);
	theyear = year;
}  
outputUpdate(1970);

d3.json('us.json', function(error, us) {

    var data = topojson.feature(us, us.objects.state).features;

	d3.csv("csv/teamstad.csv", function(error, ts) {

		var teams = [];
		var positions = [];
		var year = 0;

		for (var j = 1970; j <= 2014; j++) {
			var now = j;
			runall(now);		
		};

		function runall(now){

			d3.csv('csv/standings/standings' + now + '.csv', function(err, stands) {

				teamsLen.push(stands.length);

				stands.forEach(function(d) {
					if(now === 1970){
						teams.pushIfNotExist(d.Team, function(e){
						  return e === d.Team;
						});						
					}
					ts.forEach(function(e){	
						if(d.Team === e.Team){
							e["wins"+now] = d.W;
							e["losses"+now] = d.L;
							e["PF"+now] = d.PF;
							e["PA"+now] = d.PA;
							e["NP"+now] = d["Net Pts"];
						}
					});
				});

				if(now === 2014){
					drawAll(ts);
				}	

			});	

			d3.csv('csv/rushing/stats' + now + '.csv', function(err, stands) {
				stands.forEach(function(d) {
					ts.forEach(function(e){	
						if(d.Team === e.Team){
							e["rushrank"+now] = d.Rk;
						}
					});
				});
			});
			d3.csv('csv/passing/stats' + now + '.csv', function(err, stands) {
				stands.forEach(function(d){
					ts.forEach(function(e){	
						if(d.Team === e.Team){
							e["passrank"+now] = d.Rk;
							var a = ((parseInt(d.Comp) / parseInt(d.Att)) - 0.3) * 5;
							var b = ((parseInt(d.Yds) / parseInt(d.Att)) - 3) * 0.25
							var c = (parseInt(d.TD) / parseInt(d.Att)) * 20;
							var z = 2.375 - ((parseInt(d.Int) / parseInt(d.Att)) * 25)
							e["QBR"+now] = ((mm(a) + mm(b) + mm(c) + mm(z)) / 6) * 100;

							function mm(en){
								if( en < 0 ){
									return 0;
								}else if( en > 2.375 ){
									return 2.375;
								}else{
									return en;
								}
							}
						}
					});
				});
			});
			d3.csv('csv/defense/stats' + now + '.csv', function(err, stands) {
				stands.forEach(function(d) {
					ts.forEach(function(e){	
						if(d.Team === e.Team){
							e["defrank"+now] = d.Rk;
						}
					});
				});
			});

			d3.csv('csv/drafts/draft' + now + '.csv', function(err, draft) {

				ts.forEach(function(g){
					g["off"+now] = 0;
					g["def"+now] = 0;
					g["st"+now] = 0;
				});

				draft.forEach(function(d) {
					ts.forEach(function(e){	
						if(d.TEAM === e.Team){
							if(offense.indexOf(d.POSITION) > -1){
								e["off"+now] += (1 / Math.ceil(d.SEL / 30));

								e["odt"+now] += 1;
							}else if(defense.indexOf(d.POSITION) > -1){
								e["def"+now] += (1 / Math.ceil(d.SEL / 30));

								e["ddt"+now] += 1;
							}else{
								e["st"+now] += (1 / Math.ceil(d.SEL / 30));

								e["sdt"+now] += 1;
							}
						}
					});
				});

				if(now === 2014){
					makeLines(ts);
				}

			});	

		}

		function makeLines(arr){


		var peepee = [];
		for (var i = 2014; i >= 1970; i--) {
			var valz = d3.max(ts, function(m) { return m["QBR"+i];} );
			var teamz;
			ts.forEach(function(p){
				if(p["QBR"+i] === valz){
					teamz = p.Team;
				}
			});
			peepee.push({
				"year" : i,
				"QBR" : valz,
				"team" : teamz
			});
		};
		var cock = d3.max(peepee, function(m) { return m.QBR;} );
		var penis, ballz;
		peepee.forEach(function(p){
			if(p["QBR"] === cock){
				penis = p.team;
				ballz = p.year;
			}
		});		
		console.log( peepee, cock, penis, ballz);



			arr.forEach(function(d){
				var tys = {};
				var bodz = d3.select('#lines').append('section')
							.attr('class', function(){return d.Team;});

				var liner = bodz.append('svg')
				            .attr("width", "100%")
				            .attr("height", "100%")
				            .attr("viewBox", "0 0 960 500")
				            .attr("preserveAspectRatio", "xMinYMin meet");	

				bodz.append('h2').text(function(){return d.Team;});

				var offs = [];	
				var defs = [];	
				var sts = [];

				var passes = [];
				var rushes = [];
				var defenses = [];

				for (var i = 1970; i <= 2014; i++) {

					var df = {};
					var o = {};
					var s = {};
					var ds = {};
					var rs = {};
					var ps = {};

					df.tval = d["def"+i];
					o.tval = d["off"+i];
					s.tval = d["st"+i];

					df.yr = i;
					o.yr = i;
					s.yr = i;

					ds.tval = d["defrank"+i];
					ps.tval = d["passrank"+i];
					rs.tval = d["rushrank"+i];

					ds.yr = i;
					ps.yr = i;
					rs.yr = i;

					offs.push(o);
					defs.push(df);
					sts.push(s);

					defenses.push(ds);
					passes.push(ps);
					rushes.push(rs);

				};		

				var xScaleTot = d3.scale.linear().domain([0,offs.length - 1]).range([0,960]);
			    var minOff = d3.min(offs, function(m) { return m.tval;} );
			    var maxOff = d3.max(offs, function(m) { return m.tval;} );
			    var minDef = d3.min(defs, function(m) { return m.tval;} );
			    var maxDef = d3.max(defs, function(m) { return m.tval;} );
			    var tots = [minOff , maxOff , minDef , maxDef];


			    var minOffs = d3.min(offs, function(m) { 
			    	if( m.tval === 0 ){
			    		return 10000000000;
			    	}else{
				    	return m.tval;
			    	}
			    } );
			    var minDefs = d3.min(defs, function(m) { 
			    	if( m.tval === 0 ){
			    		return 10000000000;
			    	}else{
				    	return m.tval;
			    	}
			    } );

			    tys.Team = d.Team;
			    tys.maxO = maxOff;
			    tys.maxD = maxDef;
			    tys.minO = minOffs;
			    tys.minD = minDefs;

			    tys.maxOY = tough(offs, tys.maxO);
			    tys.maxDY = tough(defs, tys.maxD);
			    tys.minOY = tough(offs, tys.minO);
			    tys.minDY = tough(defs, tys.minD);			    

			    tys.minPR = d3.max(passes, function(m) { return parseInt(m.tval);} );
			    tys.minRR = d3.max(rushes, function(m) { return parseInt(m.tval);} );
			    tys.minDR = d3.max(defenses, function(m) { return parseInt(m.tval);} );
			    tys.maxPR = d3.min(passes, function(m) { return parseInt(m.tval);} );
			    tys.maxRR = d3.min(rushes, function(m) { return parseInt(m.tval);} );
			    tys.maxDR = d3.min(defenses, function(m) { return parseInt(m.tval);} );		

			    tys.minPRY = tough(passes, tys.minPR);
			    tys.minRRY = tough(rushes, tys.minRR);
			    tys.minDRY = tough(defenses, tys.minDR);
			    tys.maxPRY = tough(passes, tys.maxPR);
			    tys.maxRRY = tough(rushes, tys.maxRR);
			    tys.maxDRY = tough(defenses, tys.maxDR);		

			    teamYears.push(tys);

			    function tough(arr, comp){
			    	var users = [];
			    	arr.forEach(function(el){
			    		if(parseInt(el.tval) === parseInt(comp)){
			    			users.push(el.yr);
			    		}
			    	});
			    	return users;
			    }

			    var maxTot = d3.max(tots, function(m) { return m;} );
			   
			    var yScaleTot = d3.scale.linear().domain([0,(maxTot)]).range([500, 0]);
			    var opac = d3.scale.linear().domain([0, maxTot]).range([0, 1]);

			    //build x and y axes
			    var ticks = [0,maxTot/2,maxTot];

			    var yAxisTot = d3.svg.axis().scale(yScaleTot)
			            .orient("left")
			            .tickValues(ticks)
			        	.tickSize(-width);

			    var xAxisTot = d3.svg.axis().scale(xScaleTot).ticks(44).orient("bottom").tickSize(-500,0,0).tickFormat("");

			    //put x and y axes on page
			    var xAxisGroupTot = liner.append("g")
			            .attr("class", "x axis grid")
			            .attr("transform", "translate(0,500)")
			            .call(xAxisTot);
			    var yAxisGroupTot = liner.append("g")
			            .attr("class", "y axis grid")
			            .call(yAxisTot);
			    yAxisGroupTot.selectAll("text").style("opacity", "0");
			    yAxisGroupTot.selectAll(".domain").style("opacity", "0");

			    var funk = liner.append('g').attr('class', 'line-group');
				var funklines = funk.selectAll('line').data(offs).enter().append('line');		 
				var funkcircs = funk.selectAll('circle').data(offs).enter().append('circle');

				var splash = liner.append('g').attr('class', 'line-group');
				var splashlines = splash.selectAll('line').data(defs).enter().append('line');		 
				var splashcircs = splash.selectAll('circle').data(defs).enter().append('circle');		 

			    var dg = liner.append('g').attr('class', 'line-group');
			    var dglines = dg.selectAll('line').data(defenses).enter().append('line');
			    var dgcircs = dg.selectAll('circle').data(defenses).enter().append('circle');

				var rg = liner.append('g').attr('class', 'line-group');
				var rglines = rg.selectAll('line').data(rushes).enter().append('line');		
				var rgcircs = rg.selectAll('circle').data(rushes).enter().append('circle');		

				var pg = liner.append('g').attr('class', 'line-group');
				var pglines = pg.selectAll('line').data(passes).enter().append('line');		  
				var pgcircs = pg.selectAll('circle').data(passes).enter().append('circle');		  


			    splashlines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			            return yScaleTot(m.tval);
			          })
			          .attr('y2', function(m,i){
			            if(i < offs.length - 1){
			              return yScaleTot(defs[i+1].tval);
			            }else{
			              return yScaleTot(m.tval);  
			            }
			          })
			          .attr('stroke', '#0004FF')
			          .attr('stoke-width', 2);
				splashcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				            return yScaleTot(m.tval);           
				          })
				          .attr('r', 4)
				          .attr('fill', '#0004FF')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);		

			    dglines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			          	var pv;
			          	var tl = teamsLen[i];

			          	if(m.tval){
			          		pv = m.tval;
			          	}else{
			          		pv = teamsLen[i] + 1;
			          	}

			          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why1);
			          })
			          .attr('y2', function(m,i){
			          	var j;
			          	if(i === defenses.length-1){
			          		j = i;
			          	}else{
			          		j = i + 1;
			          	}
			          	var pv;
			          	var tl = teamsLen[j];

			          	if(defenses[j].tval){
			          		pv = defenses[j].tval;
			          	}else{
			          		pv = teamsLen[j] + 1;
			          	}

			          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why2);
			          })
			          .attr('stroke', '#00FF22')
			          .attr('stoke-width', 2);
				dgcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				          	var pv;
				          	var tl = teamsLen[i];

				          	if(m.tval){
				          		pv = m.tval;
				          	}else{
				          		pv = teamsLen[i] + 1;
				          	}

				          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

				            return  yScaleTot(why1);          
				          })
				          .attr('r', 4)
				          .attr('fill', '#00FF22')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);		

			    funklines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			            return yScaleTot(m.tval);
			          })
			          .attr('y2', function(m,i){
			            if(i < offs.length - 1){
			              return yScaleTot(offs[i+1].tval);
			            }else{
			              return yScaleTot(m.tval);  
			            }
			          })
			          .attr('stroke', '#FF0000')
			          .attr('stoke-width', 2);
				funkcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				            return yScaleTot(m.tval);           
				          })
				          .attr('r', 4)
				          .attr('fill', '#FF0000')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);		          

			    rglines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			          	var pv;
			          	var tl = teamsLen[i];

			          	if(m.tval){
			          		pv = m.tval;
			          	}else{
			          		pv = teamsLen[i] + 1;
			          	}

			          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why1);
			          })
			          .attr('y2', function(m,i){
			          	var j;
			          	if(i === rushes.length-1){
			          		j = i;
			          	}else{
			          		j = i + 1;
			          	}
			          	var pv;
			          	var tl = teamsLen[j];

			          	if(rushes[j].tval){
			          		pv = rushes[j].tval;
			          	}else{
			          		pv = teamsLen[j] + 1;
			          	}

			          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why2);
			          })
			          .attr('stroke', '#FFA200')
			          .attr('stoke-width', 2);
				rgcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				          	var pv;
				          	var tl = teamsLen[i];

				          	if(m.tval){
				          		pv = m.tval;
				          	}else{
				          		pv = teamsLen[i] + 1;
				          	}

				          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

				            return  yScaleTot(why1);        
				          })
				          .attr('r', 4)
				          .attr('fill', '#FFA200')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);	

			    pglines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			          	var pv;
			          	var tl = teamsLen[i];

			          	if(m.tval){
			          		pv = m.tval;
			          	}else{
			          		pv = teamsLen[i] + 1;
			          	}

			          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why1);
			          })
			          .attr('y2', function(m,i){
			          	var j;
			          	if(i === passes.length-1){
			          		j = i;
			          	}else{
			          		j = i + 1;
			          	}
			          	var pv;
			          	var tl = teamsLen[j];

			          	if(passes[j].tval){
			          		pv = passes[j].tval;
			          	}else{
			          		pv = teamsLen[j] + 1;
			          	}

			          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why2);
			          })
			          .attr('stroke', '#F6FF00')
			          .attr('stoke-width', 2);
				pgcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
					          	var pv;
					          	var tl = teamsLen[i];

					          	if(m.tval){
					          		pv = m.tval;
					          	}else{
					          		pv = teamsLen[i] + 1;
					          	}

					          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

					            return  yScaleTot(why1);     
				          })
				          .attr('r', 4)
				          .attr('fill', '#F6FF00')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);	


			});

			var mimi = d3.min(teamYears, function(m) { return m.minO;} );
			var mama = d3.max(teamYears, function(m) { return m.maxO;} );

			teamYears.forEach(function(m){
				if(m.maxO === mama){
					console.log(m.Team);
				}
			});

		}

		function drawAll(arr){
			circles = svg.selectAll("circle").data(arr);
			
			circles.enter()
			   .append("circle")
			   .attr("fill", function(d,i){
			   		return d.color1;
			   })
			   .attr("stroke", function(d,i){
			   		return d.color2;
			   })
			   .style("opacity", 0.97)
			   .attr("stroke-width", 5)
			   .on("mouseover", tip.show)
			   .on("mouseleave", tip.hide);

			for (var j = 2014; j >= 1970; j--) {
				var now = j;
				timeCircs(arr, now);		
			};
		}

		function timeCircs(arr, tim){
			var timing = tim - 1970;
			setTimeout(function(){
				d3.select('#year').text(tim);
				$('#years').val(tim);
				drawCircles(arr, tim);
			}, timing * 1000);
		}

		function drawCircles(poop, year){
			theyear = year;

			circles.each(function(d){
				var top =  parseInt(d["wins"+year]);
		   		var btm =  parseInt(d["wins"+year]) + parseInt(d["losses"+year]);
		   		var perc;

		   		if(top === 0){
		   			perc = 0.01;
		   		}else if( top > 0 ){
		   			perc = top / btm;
		   		}else{
		   			perc = 0;
		   		}
				d.r = (5 * perc) * 8;
				d.cx = projection([d.lon, d.lat])[0];
				d.cy = projection([d.lon, d.lat])[1];
			});

			circles
			   .attr("cx", function(d, i) { return d.cx; })
			   .attr("cy", function(d, i) { return d.cy; });

			// prepare data for force layout
			forceNodes = [];
			for(i = 0 ; i < poop.length ; i++ ) {
				forceNodes.push(poop[i]);
			}

			// force layout, makes circles closer.
			// This is useful when circle radius changes.
			force = d3.layout.force().gravity(3.0).size([width,height]).charge(-1).nodes(forceNodes);

			force.on("tick", function() {
				for(i = 0 ; i < poop.length ; i++) {
					for(j = 0 ; j < poop.length ; j++) { 
						it = poop[i]; 
					  	jt = poop[j]; 
						if(i==j) continue; 
						// not collide with self 
						r = it.r + jt.r; 
						// it.c is the centroid of each county and initial position of circle 
						itx = it.cx + 6; 
						ity = it.cy + 6; 
						jtx = jt.cx + 6; 
						jty = jt.cy + 6; 
						// distance between centroid of two circles 
						d = Math.sqrt( (itx - jtx) * (itx - jtx) + (ity - jty) * (ity - jty) ); 
						if(r > d) { 
							// there's a collision if distance is smaller than radius
							dr = ( r - d ) / ( d * 1 );
							it.cx = it.cx + ( itx - jtx ) * dr;
							it.cy = it.cy + ( ity - jty ) * dr;
						}
					}
				}
				circles.transition().ease("cubic")
			   	.attr("r", function(d, i){ 
			   		if(d.r < 3.5){
			   			d.r = 1;
			   		}else{
			   			return d.r - 2.5;
			   		}
			   	 })
				.attr("cx",function(it) { return it.cx; })
				.attr("cy",function(it) { return it.cy; });
			});

			force.start();

			console.log(JSON.stringify(ts));

	}

    var tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function(d) {
      	return '<div>' + 
      	d.Team + '</div><div>' +
      	d["off"+theyear] + '</div><div>' +
      	d["def"+theyear] + '</div><div>' +
      	d["wins"+theyear] + '</div>';
      });

    var circtip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function(d) {
      	return '<div>' + d.yr + '</div><div>' + d.tval + '</div>';
      });

    svg.call(tip).call(circtip);       

    var map = svg.selectAll('.state')
      .data(data)
      .enter().append('path')
      .attr('class', 'state')
      .attr('d', path)
      .attr('fill', '#fff')
      .attr('stroke', '#000')
      .attr('stroke-width', '1');

});
});



Array.prototype.inArray = function(comparer) { 
  for(var i=0; i < this.length; i++) { 
      if(comparer(this[i])) return true; 
  }
  return false; 
}; 

Array.prototype.pushIfNotExist = function(element, comparer) { 
  if (!this.inArray(comparer)) {
      this.push(element);
  }
}; 

Array.prototype.getIndexBy = function (name, value) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][name] == value) {
            return i;
        }
    }
}

Array.prototype.clone = function() {
	return this.slice(0);
};

var width = 1000,
  	height = 500,
  	theyear = 1970;

var svg = d3.select('#map').append('svg')
          .attr("width", "100%")
          .attr("height", "100%")
          .attr("viewBox", "0 0 960 500")
          .attr("preserveAspectRatio", "xMinYMin meet");

var projection = d3.geo.albersUsa().scale(1000).translate([width / 2, height / 2]);

var path = d3.geo.path().projection(projection);
var offense = [ "T" , "QB" , "WR" , "TE" , "G" , "C" , "RB" , "FB" , "OT" , "OG" ];
var opos = ["OL", "QB", "WR", "RB", "TE"];
var opostotal = [0,0,0,0,0]
var defense = [  "DE" , "OLB" , "CB" , "DT" , "ILB" , "FS" , "SS" , "LB" , "DB" , "LS" , "MLB" , "NT" , "SAF" ];
var dpos = ["LB", "DL", "DB"];
var dpostotal = [0,0,0];
var teamsLen = [];
var teamYears = [];

function outputUpdate(year) {
	d3.select('#year').text(year);
	theyear = year;
}  
outputUpdate(1970);

d3.json('us.json', function(error, us) {

    var data = topojson.feature(us, us.objects.state).features;

	d3.csv("csv/teamstad.csv", function(error, ts) {

		var teams = [];
		var positions = [];
		var year = 0;

		for (var j = 1970; j <= 2014; j++) {
			var now = j;
			runall(now);		
		};

		function runall(now){

			d3.csv('csv/standings/standings' + now + '.csv', function(err, stands) {

				teamsLen.push(stands.length);

				stands.forEach(function(d) {
					if(now === 1970){
						teams.pushIfNotExist(d.Team, function(e){
						  return e === d.Team;
						});						
					}
					ts.forEach(function(e){	
						if(d.Team === e.Team){
							e["wins"+now] = d.W;
							e["losses"+now] = d.L;
							e["PF"+now] = d.PF;
							e["PA"+now] = d.PA;
							e["NP"+now] = d["Net Pts"];
						}
					});
				});

				if(now === 2014){
					drawAll(ts);
				}	
			});	
			d3.csv('csv/rushing/stats' + now + '.csv', function(err, stands) {
				stands.forEach(function(d) {
					ts.forEach(function(e){	
						if(d.Team === e.Team){
							e["rushrank"+now] = d.Rk;
						}
					});
				});
			});
			d3.csv('csv/passing/stats' + now + '.csv', function(err, stands) {
				stands.forEach(function(d){
					ts.forEach(function(e){	
						if(d.Team === e.Team){
							e["passrank"+now] = d.Rk;
							var a = ((parseInt(d.Comp) / parseInt(d.Att)) - 0.3) * 5;
							var b = ((parseInt(d.Yds) / parseInt(d.Att)) - 3) * 0.25
							var c = (parseInt(d.TD) / parseInt(d.Att)) * 20;
							var z = 2.375 - ((parseInt(d.Int) / parseInt(d.Att)) * 25)
							e["QBR"+now] = ((mm(a) + mm(b) + mm(c) + mm(z)) / 6) * 100;

							function mm(en){
								if( en < 0 ){
									return 0;
								}else if( en > 2.375 ){
									return 2.375;
								}else{
									return en;
								}
							}
						}
					});
				});
			});
			d3.csv('csv/defense/stats' + now + '.csv', function(err, stands) {
				stands.forEach(function(d) {
					ts.forEach(function(e){	
						if(d.Team === e.Team){
							e["defrank"+now] = d.Rk;
							e["dpwrrank"+now] = (d["Pts/G"] * d["Yds/G"]) / 20;
						}
					});
				});
			});
			d3.csv('csv/drafts/draft' + now + '.csv', function(err, draft) {

				ts.forEach(function(g){
					g["off"+now] = 0;
					g["def"+now] = 0;
					g["st"+now] = 0;

					g["off"+now+"tot"] = 0;
					g["def"+now+"tot"] = 0;
					g["st"+now+"tot"] = 0;

					g["wdp"+now] = 0;
					g["wdp"+now+"tot"] = 0;

					g["qb"+now+"t"] = 0;
					g["qb"+now+"v"] = 0;

					g["odt"+now] = 0;
					g["ddt"+now] = 0;
					g["sdt"+now] = 0;					
				});

				draft.forEach(function(d) {
					ts.forEach(function(e){	
						if(d.TEAM === e.Team){
							if(offense.indexOf(d.POSITION) > -1){
								//give weighted round selection
								e["off"+now] += (1 / Math.ceil(d.SEL / 30));
								e["off"+now+"tot"] += 1;

								e["wdp"+now] += (1 / Math.ceil(d.SEL / 30));
								e["wdp"+now+"tot"] += 1;

								e["odt"+now] += 1;
								if(d.POSITION === "QB"){
									e["qb"+now+"v"] += (1 / Math.ceil(d.SEL / 30));
									e["qb"+now+"t"] += 1;
								}
							}else if(defense.indexOf(d.POSITION) > -1){
								e["def"+now] += (1 / Math.ceil(d.SEL / 30));
								e["def"+now+"tot"] += 1;

								e["wdp"+now] += (1 / Math.ceil(d.SEL / 30));
								e["wdp"+now+"tot"] += 1;

								e["ddt"+now] += 1;
							}else{
								e["st"+now] += (1 / Math.ceil(d.SEL / 30));
								e["st"+now+"tot"] += 1;

								e["wdp"+now] += (1 / Math.ceil(d.SEL / 30));
								e["wdp"+now+"tot"] += 1;

								e["sdt"+now] += 1;
							}
						}
					});
				});

				if(now === 2014){
					makeLines(ts);
				}
			});	
		}

		function makeLines(arr){

			for (var i = 1970; i <= 2014; i++) {
				ts.sort(function(a, b) {
					if(typeof a["dpwrrank"+i] === 'undefined'){
						a["dpwrrank"+i] = 1000000;
					}
					if(typeof b["dpwrrank"+i] === 'undefined'){
						b["dpwrrank"+i] = 1000000;
					}					
				    return a["dpwrrank"+i] - b["dpwrrank"+i];
				});
				ts.forEach(function(element, index){
					element["dpr"+i] = index + 1;
					// console.log(i, index + 1, i-1, element["dpr"+(i-1)], element["dpwrrank"+i], element.Team, element['def'+i]);
				});
			};

			var peepee = [];
			var fiveyrav = {};
			for (var i = 1970; i <= 2009; i++) {
				fiveyrav["year"+i] = {};
			}
			ts.forEach(function(element, index){
				for (var i = 1970; i <= 2009; i++) {
					element["fiveyr"+i] = {};

					fiveyrav["year"+i][element.Team] = {};
					fiveyrav["year"+i][element.Team].ODA = element["off"+i];
					fiveyrav["year"+i][element.Team].ODT = element["odt"+i];

					fiveyrav["year"+i][element.Team].DDA = element["def"+i];
					fiveyrav["year"+i][element.Team].DDT = element["ddt"+i];

					fiveyrav["year"+i][element.Team].SDA = element["st"+i];
					fiveyrav["year"+i][element.Team].SDT = element["sdt"+i];

					fiveyrav["year"+i][element.Team].RR = getAverage(i, 5, "rushrank", element);
					fiveyrav["year"+i][element.Team].PR = getAverage(i, 5, "passrank", element);
					fiveyrav["year"+i][element.Team].DR = getAverage(i, 5, "dpwrrank", element);
					fiveyrav["year"+i][element.Team].DPR = getAverage(i, 5, "dpr", element);
					fiveyrav["year"+i][element.Team].WINS = getAverage(i, 5, "wins", element);
					fiveyrav["year"+i][element.Team].LOSSES = getAverage(i, 5, "losses", element);

					element["fiveyr"+i].RR = getAverage(i, 5, "rushrank", element);
					element["fiveyr"+i].PR = getAverage(i, 5, "passrank", element);
					element["fiveyr"+i].DR = getAverage(i, 5, "dpwrrank", element);
					element["fiveyr"+i].DPR = getAverage(i, 5, "dpr", element);
					element["fiveyr"+i].WINS = getAverage(i, 5, "wins", element);
					element["fiveyr"+i].LOSSES = getAverage(i, 5, "losses", element);					

					if(fiveyrav["year"+i][element.Team].ODT !== 0 && fiveyrav["year"+i][element.Team].DDT !== 0){
						fiveyrav["year"+i][element.Team].DDAV = fiveyrav["year"+i][element.Team].DDA / fiveyrav["year"+i][element.Team].DDT;
						fiveyrav["year"+i][element.Team].ODAV = fiveyrav["year"+i][element.Team].ODA / fiveyrav["year"+i][element.Team].ODT;

						element["fiveyr"+i].DDAV = fiveyrav["year"+i][element.Team].DDA / fiveyrav["year"+i][element.Team].DDT;
						element["fiveyr"+i].ODAV = fiveyrav["year"+i][element.Team].ODA / fiveyrav["year"+i][element.Team].ODT;						
					}else{
						fiveyrav["year"+i][element.Team].DDAV = 0;
						fiveyrav["year"+i][element.Team].ODAV = 0;	

						element["fiveyr"+i].DDAV = 0;
						element["fiveyr"+i].ODAV = 0;												
					}
				}
			});
			function getAverage( year, spans, prop, element ){

				var eval = 0;
				var modspan = spans;

				for (var i = year; i < year + spans; i++) {
					if( element[prop + i] > -1 ){
						eval += parseFloat(element[prop + i]);
					}else{
						modspan -= 1;
					}
				};

				if(modspan > 0){
					return eval / modspan;
				}else{
					return 0;
				}

			}
			for (var i = 2014; i >= 1970; i--) {




				var valz = d3.max(ts, function(m) { return m["QBR"+i];} );
				var teamz;
				ts.forEach(function(p){
					if(p["QBR"+i] === valz){
						teamz = p.Team;
					}
				});
				peepee.push({
					"year" : i,
					"QBR" : valz,
					"team" : teamz
				});
			};
			console.log(fiveyrav);
			var cock = d3.max(peepee, function(m) { return m.QBR;} );
			var penis, ballz;
			peepee.forEach(function(p){
				if(p["QBR"] === cock){
					penis = p.team;
					ballz = p.year;
				}
			});		
			console.log( peepee, cock, penis, ballz);



			arr.forEach(function(d){
				var tys = {};
				var bodz = d3.select('#lines').append('section')
							.attr('class', function(){return d.Team;});

				var liner = bodz.append('svg')
				            .attr("width", "100%")
				            .attr("height", "100%")
				            .attr("viewBox", "0 0 960 500")
				            .attr("preserveAspectRatio", "xMinYMin meet");	

				bodz.append('h2').text(function(){return d.Team;});

				var offs = [];	
				var defs = [];	
				var sts = [];

				var passes = [];
				var rushes = [];
				var defenses = [];

				for (var i = 1970; i <= 2014; i++) {

					var df = {};
					var o = {};
					var s = {};
					var ds = {};
					var rs = {};
					var ps = {};

					df.tval = d["def"+i];
					o.tval = d["off"+i];
					s.tval = d["st"+i];

					df.yr = i;
					o.yr = i;
					s.yr = i;

					ds.tval = d["dpr"+i];
					ps.tval = d["passrank"+i];
					rs.tval = d["rushrank"+i];

					ds.yr = i;
					ps.yr = i;
					rs.yr = i;

					offs.push(o);
					defs.push(df);
					sts.push(s);

					defenses.push(ds);
					passes.push(ps);
					rushes.push(rs);

				};		

				var xScaleTot = d3.scale.linear().domain([0,offs.length - 1]).range([0,960]);
			    var minOff = d3.min(offs, function(m) { return m.tval;} );
			    var maxOff = d3.max(offs, function(m) { return m.tval;} );
			    var minDef = d3.min(defs, function(m) { return m.tval;} );
			    var maxDef = d3.max(defs, function(m) { return m.tval;} );
			    var tots = [minOff , maxOff , minDef , maxDef];


			    var minOffs = d3.min(offs, function(m) { 
			    	if( m.tval === 0 ){
			    		return 10000000000;
			    	}else{
				    	return m.tval;
			    	}
			    } );
			    var minDefs = d3.min(defs, function(m) { 
			    	if( m.tval === 0 ){
			    		return 10000000000;
			    	}else{
				    	return m.tval;
			    	}
			    } );

			    tys.Team = d.Team;
			    tys.maxO = maxOff;
			    tys.maxD = maxDef;
			    tys.minO = minOffs;
			    tys.minD = minDefs;

			    tys.maxOY = tough(offs, tys.maxO);
			    tys.maxDY = tough(defs, tys.maxD);
			    tys.minOY = tough(offs, tys.minO);
			    tys.minDY = tough(defs, tys.minD);			    

			    tys.minPR = d3.max(passes, function(m) { return parseInt(m.tval);} );
			    tys.minRR = d3.max(rushes, function(m) { return parseInt(m.tval);} );
			    tys.minDR = d3.max(defenses, function(m) { return parseInt(m.tval);} );
			    tys.maxPR = d3.min(passes, function(m) { return parseInt(m.tval);} );
			    tys.maxRR = d3.min(rushes, function(m) { return parseInt(m.tval);} );
			    tys.maxDR = d3.min(defenses, function(m) { return parseInt(m.tval);} );		

			    tys.minPRY = tough(passes, tys.minPR);
			    tys.minRRY = tough(rushes, tys.minRR);
			    tys.minDRY = tough(defenses, tys.minDR);
			    tys.maxPRY = tough(passes, tys.maxPR);
			    tys.maxRRY = tough(rushes, tys.maxRR);
			    tys.maxDRY = tough(defenses, tys.maxDR);		

			    teamYears.push(tys);

			    function tough(arr, comp){
			    	var users = [];
			    	arr.forEach(function(el){
			    		if(parseInt(el.tval) === parseInt(comp)){
			    			users.push(el.yr);
			    		}
			    	});
			    	return users;
			    }

			    var maxTot = d3.max(tots, function(m) { return m;} );
			   
			    var yScaleTot = d3.scale.linear().domain([0,(maxTot)]).range([500, 0]);
			    var opac = d3.scale.linear().domain([0, maxTot]).range([0, 1]);

			    //build x and y axes
			    var ticks = [0,maxTot/2,maxTot];

			    var yAxisTot = d3.svg.axis().scale(yScaleTot)
			            .orient("left")
			            .tickValues(ticks)
			        	.tickSize(-width);

			    var xAxisTot = d3.svg.axis().scale(xScaleTot).ticks(44).orient("bottom").tickSize(-500,0,0).tickFormat("");

			    //put x and y axes on page
			    var xAxisGroupTot = liner.append("g")
			            .attr("class", "x axis grid")
			            .attr("transform", "translate(0,500)")
			            .call(xAxisTot);
			    var yAxisGroupTot = liner.append("g")
			            .attr("class", "y axis grid")
			            .call(yAxisTot);
			    yAxisGroupTot.selectAll("text").style("opacity", "0");
			    yAxisGroupTot.selectAll(".domain").style("opacity", "0");

				// var funk = liner.append('g').attr('class', 'line-group');
				// var funklines = funk.selectAll('line').data(offs).enter().append('line');		 
				// var funkcircs = funk.selectAll('circle').data(offs).enter().append('circle');

				var splash = liner.append('g').attr('class', 'line-group');
				var splashlines = splash.selectAll('line').data(defs).enter().append('line');		 
				var splashcircs = splash.selectAll('circle').data(defs).enter().append('circle');		 

			    var dg = liner.append('g').attr('class', 'line-group');
			    var dglines = dg.selectAll('line').data(defenses).enter().append('line');
			    var dgcircs = dg.selectAll('circle').data(defenses).enter().append('circle');

				// var rg = liner.append('g').attr('class', 'line-group');
				// var rglines = rg.selectAll('line').data(rushes).enter().append('line');		
				// var rgcircs = rg.selectAll('circle').data(rushes).enter().append('circle');		

				// var pg = liner.append('g').attr('class', 'line-group');
				// var pglines = pg.selectAll('line').data(passes).enter().append('line');		  
				// var pgcircs = pg.selectAll('circle').data(passes).enter().append('circle');		  


			    splashlines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			            return yScaleTot(m.tval);
			          })
			          .attr('y2', function(m,i){
			            if(i < offs.length - 1){
			              return yScaleTot(defs[i+1].tval);
			            }else{
			              return yScaleTot(m.tval);  
			            }
			          })
			          .attr('stroke', '#0004FF')
			          .attr('stoke-width', 2);
				splashcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				            return yScaleTot(m.tval);           
				          })
				          .attr('r', 4)
				          .attr('fill', '#0004FF')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);		

			    dglines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			          	var pv;
			          	var tl = teamsLen[i];

			          	if(m.tval){
			          		pv = m.tval;
			          	}else{
			          		pv = teamsLen[i] + 1;
			          	}

			          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why1);
			          })
			          .attr('y2', function(m,i){
			          	var j;
			          	if(i === defenses.length-1){
			          		j = i;
			          	}else{
			          		j = i + 1;
			          	}
			          	var pv;
			          	var tl = teamsLen[j];

			          	if(defenses[j].tval){
			          		pv = defenses[j].tval;
			          	}else{
			          		pv = teamsLen[j] + 1;
			          	}

			          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why2);
			          })
			          .attr('stroke', '#00FF22')
			          .attr('stoke-width', 2);
				dgcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				          	var pv;
				          	var tl = teamsLen[i];

				          	if(m.tval){
				          		pv = m.tval;
				          	}else{
				          		pv = teamsLen[i] + 1;
				          	}

				          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

				            return  yScaleTot(why1);          
				          })
				          .attr('r', 4)
				          .attr('fill', '#00FF22')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);		

			 //    funklines.attr('x1', function(m,i){
			 //            return xScaleTot(i);
			 //          })
			 //          .attr('x2', function(m,i){
			 //            if(i < offs.length - 1){
			 //              return xScaleTot(i + 1);
			 //            }else{
			 //              return xScaleTot(i);
			 //            }            
			 //          })
			 //          .attr('y1', function(m,i){
			 //            return yScaleTot(m.tval);
			 //          })
			 //          .attr('y2', function(m,i){
			 //            if(i < offs.length - 1){
			 //              return yScaleTot(offs[i+1].tval);
			 //            }else{
			 //              return yScaleTot(m.tval);  
			 //            }
			 //          })
			 //          .attr('stroke', '#FF0000')
			 //          .attr('stoke-width', 2);
				// funkcircs.attr('cx', function(m,i){
				//             return xScaleTot(i);
				//           })
				//           .attr('cy', function(m,i){
				//             return yScaleTot(m.tval);           
				//           })
				//           .attr('r', 4)
				//           .attr('fill', '#FF0000')
				//           .style("opacity", function(m){return opac(m.tval);})
				//           .on('mouseenter', circtip.show)
				//           .on('mouseleave', circtip.hide);		          

			 //    rglines.attr('x1', function(m,i){
			 //            return xScaleTot(i);
			 //          })
			 //          .attr('x2', function(m,i){
			 //            if(i < offs.length - 1){
			 //              return xScaleTot(i + 1);
			 //            }else{
			 //              return xScaleTot(i);
			 //            }            
			 //          })
			 //          .attr('y1', function(m,i){
			 //          	var pv;
			 //          	var tl = teamsLen[i];

			 //          	if(m.tval){
			 //          		pv = m.tval;
			 //          	}else{
			 //          		pv = teamsLen[i] + 1;
			 //          	}

			 //          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			 //            return  yScaleTot(why1);
			 //          })
			 //          .attr('y2', function(m,i){
			 //          	var j;
			 //          	if(i === rushes.length-1){
			 //          		j = i;
			 //          	}else{
			 //          		j = i + 1;
			 //          	}
			 //          	var pv;
			 //          	var tl = teamsLen[j];

			 //          	if(rushes[j].tval){
			 //          		pv = rushes[j].tval;
			 //          	}else{
			 //          		pv = teamsLen[j] + 1;
			 //          	}

			 //          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			 //            return  yScaleTot(why2);
			 //          })
			 //          .attr('stroke', '#FFA200')
			 //          .attr('stoke-width', 2);
				// rgcircs.attr('cx', function(m,i){
				//             return xScaleTot(i);
				//           })
				//           .attr('cy', function(m,i){
				//           	var pv;
				//           	var tl = teamsLen[i];

				//           	if(m.tval){
				//           		pv = m.tval;
				//           	}else{
				//           		pv = teamsLen[i] + 1;
				//           	}

				//           	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

				//             return  yScaleTot(why1);        
				//           })
				//           .attr('r', 4)
				//           .attr('fill', '#FFA200')
				//           .style("opacity", function(m){return opac(m.tval);})
				//           .on('mouseenter', circtip.show)
				//           .on('mouseleave', circtip.hide);	

			 //    pglines.attr('x1', function(m,i){
			 //            return xScaleTot(i);
			 //          })
			 //          .attr('x2', function(m,i){
			 //            if(i < offs.length - 1){
			 //              return xScaleTot(i + 1);
			 //            }else{
			 //              return xScaleTot(i);
			 //            }            
			 //          })
			 //          .attr('y1', function(m,i){
			 //          	var pv;
			 //          	var tl = teamsLen[i];

			 //          	if(m.tval){
			 //          		pv = m.tval;
			 //          	}else{
			 //          		pv = teamsLen[i] + 1;
			 //          	}

			 //          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			 //            return  yScaleTot(why1);
			 //          })
			 //          .attr('y2', function(m,i){
			 //          	var j;
			 //          	if(i === passes.length-1){
			 //          		j = i;
			 //          	}else{
			 //          		j = i + 1;
			 //          	}
			 //          	var pv;
			 //          	var tl = teamsLen[j];

			 //          	if(passes[j].tval){
			 //          		pv = passes[j].tval;
			 //          	}else{
			 //          		pv = teamsLen[j] + 1;
			 //          	}

			 //          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			 //            return  yScaleTot(why2);
			 //          })
			 //          .attr('stroke', '#F6FF00')
			 //          .attr('stoke-width', 2);
				// pgcircs.attr('cx', function(m,i){
				//             return xScaleTot(i);
				//           })
				//           .attr('cy', function(m,i){
				// 	          	var pv;
				// 	          	var tl = teamsLen[i];

				// 	          	if(m.tval){
				// 	          		pv = m.tval;
				// 	          	}else{
				// 	          		pv = teamsLen[i] + 1;
				// 	          	}

				// 	          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

				// 	            return  yScaleTot(why1);     
				//           })
				//           .attr('r', 4)
				//           .attr('fill', '#F6FF00')
				//           .style("opacity", function(m){return opac(m.tval);})
				//           .on('mouseenter', circtip.show)
				//           .on('mouseleave', circtip.hide);	


			});

			var mimi = d3.min(teamYears, function(m) { return m.minO;} );
			var mama = d3.max(teamYears, function(m) { return m.maxO;} );

			teamYears.forEach(function(m){
				if(m.maxO === mama){
					console.log(m.Team);
				}
			});

			// console.log(ts);
			// d3.select("body").append('p').text(JSON.stringify(ts));
			// console.log("fuck");
		}

		function drawAll(arr){
			circles = svg.selectAll("circle").data(arr);
			
			circles.enter()
			   .append("circle")
			   .attr("fill", function(d,i){
			   		return d.color1;
			   })
			   .attr("stroke", function(d,i){
			   		return d.color2;
			   })
			   .style("opacity", 0.97)
			   .attr("stroke-width", 5)
			   .on("mouseover", tip.show)
			   .on("mouseleave", tip.hide);

			for (var j = 2014; j >= 1970; j--) {
				var now = j;
				timeCircs(arr, now);		
			};
		}

		function timeCircs(arr, tim){
			var timing = tim - 1970;
			setTimeout(function(){
				d3.select('#year').text(tim);
				$('#years').val(tim);
				drawCircles(arr, tim);
			}, timing * 1000);
		}

		function drawCircles(poop, year){
			theyear = year;

			circles.each(function(d){
				var top =  parseInt(d["wins"+year]);
		   		var btm =  parseInt(d["wins"+year]) + parseInt(d["losses"+year]);
		   		var perc;

		   		if(top === 0){
		   			perc = 0.01;
		   		}else if( top > 0 ){
		   			perc = top / btm;
		   		}else{
		   			perc = 0;
		   		}
				d.r = (5 * perc) * 8;
				d.cx = projection([d.lon, d.lat])[0];
				d.cy = projection([d.lon, d.lat])[1];
			});

			circles
			   .attr("cx", function(d, i) { return d.cx; })
			   .attr("cy", function(d, i) { return d.cy; });

			// prepare data for force layout
			forceNodes = [];
			for(i = 0 ; i < poop.length ; i++ ) {
				forceNodes.push(poop[i]);
			}

			// force layout, makes circles closer.
			// This is useful when circle radius changes.
			force = d3.layout.force().gravity(3.0).size([width,height]).charge(-1).nodes(forceNodes);

			force.on("tick", function() {
				for(i = 0 ; i < poop.length ; i++) {
					for(j = 0 ; j < poop.length ; j++) { 
						it = poop[i]; 
					  	jt = poop[j]; 
						if(i==j) continue; 
						// not collide with self 
						r = it.r + jt.r; 
						// it.c is the centroid of each county and initial position of circle 
						itx = it.cx + 6; 
						ity = it.cy + 6; 
						jtx = jt.cx + 6; 
						jty = jt.cy + 6; 
						// distance between centroid of two circles 
						d = Math.sqrt( (itx - jtx) * (itx - jtx) + (ity - jty) * (ity - jty) ); 
						if(r > d) { 
							// there's a collision if distance is smaller than radius
							dr = ( r - d ) / ( d * 1 );
							it.cx = it.cx + ( itx - jtx ) * dr;
							it.cy = it.cy + ( ity - jty ) * dr;
						}
					}
				}
				circles.transition().duration(250).ease("cubic")
			   	.attr("r", function(d, i){ 
			   		if(d.r < 3.5){
			   			d.r = 1;
			   		}else{
			   			return d.r - 2.5;
			   		}
			   	 })
				.attr("cx",function(it) { return it.cx; })
				.attr("cy",function(it) { return it.cy; });
			});


			force.start();
		}

	    var tip = d3.tip()
	      .attr('class', 'd3-tip')
	      .offset([-10, 0])
	      .html(function(d) {
	      	return '<div>Team : ' + 
	      	d.Team + '</div><div>O-Draft Stock : ' +
	      	Math.round(d["off"+theyear] * 1000) / 1000 + '</div><div>D-Draft Stock : ' +
	      	Math.round(d["def"+theyear] * 1000) / 1000 + '</div><div>Wins : ' +
	      	d["wins"+theyear] + '</div>';
	      });

	    var circtip = d3.tip()
	      .attr('class', 'd3-tip')
	      .offset([-10, 0])
	      .html(function(d) {
	      	return '<div>' + d.yr + '</div><div>' + d.tval + '</div>';
	      });

	    svg.call(tip).call(circtip);       

	    var map = svg.selectAll('.state')
	      .data(data)
	      .enter().append('path')
	      .attr('class', 'state')
	      .attr('d', path)
	      .attr('fill', '#fff')
	      .attr('stroke', '#000')
	      .attr('stroke-width', '1');

	});
});



Array.prototype.inArray = function(comparer) { 
  for(var i=0; i < this.length; i++) { 
      if(comparer(this[i])) return true; 
  }
  return false; 
}; 

Array.prototype.pushIfNotExist = function(element, comparer) { 
  if (!this.inArray(comparer)) {
      this.push(element);
  }
}; 

Array.prototype.getIndexBy = function (name, value) {
    for (var i = 0; i < this.length; i++) {
        if (this[i][name] == value) {
            return i;
        }
    }
}

Array.prototype.clone = function() {
	return this.slice(0);
};

var width = 1000,
  	height = 500,
  	theyear = 1970;

var svg = d3.select('#map').append('svg')
          .attr("width", "100%")
          .attr("height", "100%")
          .attr("viewBox", "0 0 960 500")
          .attr("preserveAspectRatio", "xMinYMin meet");

var projection = d3.geo.albersUsa().scale(1000).translate([width / 2, height / 2]);

var path = d3.geo.path().projection(projection);
var offense = [ "T" , "QB" , "WR" , "TE" , "G" , "C" , "RB" , "FB" , "OT" , "OG" ];
var opos = ["OL", "QB", "WR", "RB", "TE"];
var opostotal = [0,0,0,0,0]
var defense = [  "DE" , "OLB" , "CB" , "DT" , "ILB" , "FS" , "SS" , "LB" , "DB" , "LS" , "MLB" , "NT" , "SAF" ];
var dpos = ["LB", "DL", "DB"];
var dpostotal = [0,0,0];
var teamsLen = [];
var teamYears = [];

function outputUpdate(year) {
	d3.select('#year').text(year);
	theyear = year;
}  
outputUpdate(1970);

d3.json('us.json', function(error, us) {

    var data = topojson.feature(us, us.objects.state).features;

	d3.csv("csv/teamstad.csv", function(error, ts) {

		function makeLines(arr){


		var peepee = [];
		for (var i = 2014; i >= 1970; i--) {
			var valz = d3.max(ts, function(m) { return m["QBR"+i];} );
			var teamz;
			ts.forEach(function(p){
				if(p["QBR"+i] === valz){
					teamz = p.Team;
				}
			});
			peepee.push({
				"year" : i,
				"QBR" : valz,
				"team" : teamz
			});
		};
		var cock = d3.max(peepee, function(m) { return m.QBR;} );
		var penis, ballz;
		peepee.forEach(function(p){
			if(p["QBR"] === cock){
				penis = p.team;
				ballz = p.year;
			}
		});		
		console.log( peepee, cock, penis, ballz);



			arr.forEach(function(d){
				var tys = {};
				var bodz = d3.select('#lines').append('section')
							.attr('class', function(){return d.Team;});

				var liner = bodz.append('svg')
				            .attr("width", "100%")
				            .attr("height", "100%")
				            .attr("viewBox", "0 0 960 500")
				            .attr("preserveAspectRatio", "xMinYMin meet");	

				bodz.append('h2').text(function(){return d.Team;});

				var offs = [];	
				var defs = [];	
				var sts = [];

				var passes = [];
				var rushes = [];
				var defenses = [];

				for (var i = 1970; i <= 2014; i++) {

					var df = {};
					var o = {};
					var s = {};
					var ds = {};
					var rs = {};
					var ps = {};

					df.tval = d["def"+i];
					o.tval = d["off"+i];
					s.tval = d["st"+i];

					df.yr = i;
					o.yr = i;
					s.yr = i;

					ds.tval = d["defrank"+i];
					ps.tval = d["passrank"+i];
					rs.tval = d["rushrank"+i];

					ds.yr = i;
					ps.yr = i;
					rs.yr = i;

					offs.push(o);
					defs.push(df);
					sts.push(s);

					defenses.push(ds);
					passes.push(ps);
					rushes.push(rs);

				};		

				var xScaleTot = d3.scale.linear().domain([0,offs.length - 1]).range([0,960]);
			    var minOff = d3.min(offs, function(m) { return m.tval;} );
			    var maxOff = d3.max(offs, function(m) { return m.tval;} );
			    var minDef = d3.min(defs, function(m) { return m.tval;} );
			    var maxDef = d3.max(defs, function(m) { return m.tval;} );
			    var tots = [minOff , maxOff , minDef , maxDef];


			    var minOffs = d3.min(offs, function(m) { 
			    	if( m.tval === 0 ){
			    		return 10000000000;
			    	}else{
				    	return m.tval;
			    	}
			    } );
			    var minDefs = d3.min(defs, function(m) { 
			    	if( m.tval === 0 ){
			    		return 10000000000;
			    	}else{
				    	return m.tval;
			    	}
			    } );

			    tys.Team = d.Team;
			    tys.maxO = maxOff;
			    tys.maxD = maxDef;
			    tys.minO = minOffs;
			    tys.minD = minDefs;

			    tys.maxOY = tough(offs, tys.maxO);
			    tys.maxDY = tough(defs, tys.maxD);
			    tys.minOY = tough(offs, tys.minO);
			    tys.minDY = tough(defs, tys.minD);			    

			    tys.minPR = d3.max(passes, function(m) { return parseInt(m.tval);} );
			    tys.minRR = d3.max(rushes, function(m) { return parseInt(m.tval);} );
			    tys.minDR = d3.max(defenses, function(m) { return parseInt(m.tval);} );
			    tys.maxPR = d3.min(passes, function(m) { return parseInt(m.tval);} );
			    tys.maxRR = d3.min(rushes, function(m) { return parseInt(m.tval);} );
			    tys.maxDR = d3.min(defenses, function(m) { return parseInt(m.tval);} );		

			    tys.minPRY = tough(passes, tys.minPR);
			    tys.minRRY = tough(rushes, tys.minRR);
			    tys.minDRY = tough(defenses, tys.minDR);
			    tys.maxPRY = tough(passes, tys.maxPR);
			    tys.maxRRY = tough(rushes, tys.maxRR);
			    tys.maxDRY = tough(defenses, tys.maxDR);		

			    teamYears.push(tys);

			    function tough(arr, comp){
			    	var users = [];
			    	arr.forEach(function(el){
			    		if(parseInt(el.tval) === parseInt(comp)){
			    			users.push(el.yr);
			    		}
			    	});
			    	return users;
			    }

			    var maxTot = d3.max(tots, function(m) { return m;} );
			   
			    var yScaleTot = d3.scale.linear().domain([0,(maxTot)]).range([500, 0]);
			    var opac = d3.scale.linear().domain([0, maxTot]).range([0, 1]);

			    //build x and y axes
			    var ticks = [0,maxTot/2,maxTot];

			    var yAxisTot = d3.svg.axis().scale(yScaleTot)
			            .orient("left")
			            .tickValues(ticks)
			        	.tickSize(-width);

			    var xAxisTot = d3.svg.axis().scale(xScaleTot).ticks(44).orient("bottom").tickSize(-500,0,0).tickFormat("");

			    //put x and y axes on page
			    var xAxisGroupTot = liner.append("g")
			            .attr("class", "x axis grid")
			            .attr("transform", "translate(0,500)")
			            .call(xAxisTot);
			    var yAxisGroupTot = liner.append("g")
			            .attr("class", "y axis grid")
			            .call(yAxisTot);
			    yAxisGroupTot.selectAll("text").style("opacity", "0");
			    yAxisGroupTot.selectAll(".domain").style("opacity", "0");

			    var funk = liner.append('g').attr('class', 'line-group');
				var funklines = funk.selectAll('line').data(offs).enter().append('line');		 
				var funkcircs = funk.selectAll('circle').data(offs).enter().append('circle');

				var splash = liner.append('g').attr('class', 'line-group');
				var splashlines = splash.selectAll('line').data(defs).enter().append('line');		 
				var splashcircs = splash.selectAll('circle').data(defs).enter().append('circle');		 

			    var dg = liner.append('g').attr('class', 'line-group');
			    var dglines = dg.selectAll('line').data(defenses).enter().append('line');
			    var dgcircs = dg.selectAll('circle').data(defenses).enter().append('circle');

				var rg = liner.append('g').attr('class', 'line-group');
				var rglines = rg.selectAll('line').data(rushes).enter().append('line');		
				var rgcircs = rg.selectAll('circle').data(rushes).enter().append('circle');		

				var pg = liner.append('g').attr('class', 'line-group');
				var pglines = pg.selectAll('line').data(passes).enter().append('line');		  
				var pgcircs = pg.selectAll('circle').data(passes).enter().append('circle');		  


			    splashlines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			            return yScaleTot(m.tval);
			          })
			          .attr('y2', function(m,i){
			            if(i < offs.length - 1){
			              return yScaleTot(defs[i+1].tval);
			            }else{
			              return yScaleTot(m.tval);  
			            }
			          })
			          .attr('stroke', '#0004FF')
			          .attr('stoke-width', 2);
				splashcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				            return yScaleTot(m.tval);           
				          })
				          .attr('r', 4)
				          .attr('fill', '#0004FF')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);		

			    dglines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			          	var pv;
			          	var tl = teamsLen[i];

			          	if(m.tval){
			          		pv = m.tval;
			          	}else{
			          		pv = teamsLen[i] + 1;
			          	}

			          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why1);
			          })
			          .attr('y2', function(m,i){
			          	var j;
			          	if(i === defenses.length-1){
			          		j = i;
			          	}else{
			          		j = i + 1;
			          	}
			          	var pv;
			          	var tl = teamsLen[j];

			          	if(defenses[j].tval){
			          		pv = defenses[j].tval;
			          	}else{
			          		pv = teamsLen[j] + 1;
			          	}

			          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why2);
			          })
			          .attr('stroke', '#00FF22')
			          .attr('stoke-width', 2);
				dgcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				          	var pv;
				          	var tl = teamsLen[i];

				          	if(m.tval){
				          		pv = m.tval;
				          	}else{
				          		pv = teamsLen[i] + 1;
				          	}

				          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

				            return  yScaleTot(why1);          
				          })
				          .attr('r', 4)
				          .attr('fill', '#00FF22')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);		

			    funklines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			            return yScaleTot(m.tval);
			          })
			          .attr('y2', function(m,i){
			            if(i < offs.length - 1){
			              return yScaleTot(offs[i+1].tval);
			            }else{
			              return yScaleTot(m.tval);  
			            }
			          })
			          .attr('stroke', '#FF0000')
			          .attr('stoke-width', 2);
				funkcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				            return yScaleTot(m.tval);           
				          })
				          .attr('r', 4)
				          .attr('fill', '#FF0000')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);		          

			    rglines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			          	var pv;
			          	var tl = teamsLen[i];

			          	if(m.tval){
			          		pv = m.tval;
			          	}else{
			          		pv = teamsLen[i] + 1;
			          	}

			          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why1);
			          })
			          .attr('y2', function(m,i){
			          	var j;
			          	if(i === rushes.length-1){
			          		j = i;
			          	}else{
			          		j = i + 1;
			          	}
			          	var pv;
			          	var tl = teamsLen[j];

			          	if(rushes[j].tval){
			          		pv = rushes[j].tval;
			          	}else{
			          		pv = teamsLen[j] + 1;
			          	}

			          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why2);
			          })
			          .attr('stroke', '#FFA200')
			          .attr('stoke-width', 2);
				rgcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
				          	var pv;
				          	var tl = teamsLen[i];

				          	if(m.tval){
				          		pv = m.tval;
				          	}else{
				          		pv = teamsLen[i] + 1;
				          	}

				          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

				            return  yScaleTot(why1);        
				          })
				          .attr('r', 4)
				          .attr('fill', '#FFA200')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);	

			    pglines.attr('x1', function(m,i){
			            return xScaleTot(i);
			          })
			          .attr('x2', function(m,i){
			            if(i < offs.length - 1){
			              return xScaleTot(i + 1);
			            }else{
			              return xScaleTot(i);
			            }            
			          })
			          .attr('y1', function(m,i){
			          	var pv;
			          	var tl = teamsLen[i];

			          	if(m.tval){
			          		pv = m.tval;
			          	}else{
			          		pv = teamsLen[i] + 1;
			          	}

			          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why1);
			          })
			          .attr('y2', function(m,i){
			          	var j;
			          	if(i === passes.length-1){
			          		j = i;
			          	}else{
			          		j = i + 1;
			          	}
			          	var pv;
			          	var tl = teamsLen[j];

			          	if(passes[j].tval){
			          		pv = passes[j].tval;
			          	}else{
			          		pv = teamsLen[j] + 1;
			          	}

			          	var why2 = ( ( tl - pv + 1 ) / tl) * maxTot;

			            return  yScaleTot(why2);
			          })
			          .attr('stroke', '#F6FF00')
			          .attr('stoke-width', 2);
				pgcircs.attr('cx', function(m,i){
				            return xScaleTot(i);
				          })
				          .attr('cy', function(m,i){
					          	var pv;
					          	var tl = teamsLen[i];

					          	if(m.tval){
					          		pv = m.tval;
					          	}else{
					          		pv = teamsLen[i] + 1;
					          	}

					          	var why1 = ( ( tl - pv + 1 ) / tl) * maxTot;

					            return  yScaleTot(why1);     
				          })
				          .attr('r', 4)
				          .attr('fill', '#F6FF00')
				          .style("opacity", function(m){return opac(m.tval);})
				          .on('mouseenter', circtip.show)
				          .on('mouseleave', circtip.hide);	


			});

			var mimi = d3.min(teamYears, function(m) { return m.minO;} );
			var mama = d3.max(teamYears, function(m) { return m.maxO;} );

			teamYears.forEach(function(m){
				if(m.maxO === mama){
					console.log(m.Team);
				}
			});

		}

		function drawAll(arr){
			circles = svg.selectAll("circle").data(arr);
			
			circles.enter()
			   .append("circle")
			   .attr("fill", function(d,i){
			   		return d.color1;
			   })
			   .attr("stroke", function(d,i){
			   		return d.color2;
			   })
			   .style("opacity", 0.97)
			   .attr("stroke-width", 5)
			   .on("mouseover", tip.show)
			   .on("mouseleave", tip.hide);

			for (var j = 2014; j >= 1970; j--) {
				var now = j;
				timeCircs(arr, now);		
			};
		}

		function timeCircs(arr, tim){
			var timing = tim - 1970;
			setTimeout(function(){
				d3.select('#year').text(tim);
				$('#years').val(tim);
				drawCircles(arr, tim);
			}, timing * 1000);
		}

		function drawCircles(poop, year){
			theyear = year;

			circles.each(function(d){
				var top =  parseInt(d["wins"+year]);
		   		var btm =  parseInt(d["wins"+year]) + parseInt(d["losses"+year]);
		   		var perc;

		   		if(top === 0){
		   			perc = 0.01;
		   		}else if( top > 0 ){
		   			perc = top / btm;
		   		}else{
		   			perc = 0;
		   		}
				d.r = (5 * perc) * 8;
				d.cx = projection([d.lon, d.lat])[0];
				d.cy = projection([d.lon, d.lat])[1];
			});

			circles
			   .attr("cx", function(d, i) { return d.cx; })
			   .attr("cy", function(d, i) { return d.cy; });

			// prepare data for force layout
			forceNodes = [];
			for(i = 0 ; i < poop.length ; i++ ) {
				forceNodes.push(poop[i]);
			}

			// force layout, makes circles closer.
			// This is useful when circle radius changes.
			force = d3.layout.force().gravity(3.0).size([width,height]).charge(-1).nodes(forceNodes);

			force.on("tick", function() {
				for(i = 0 ; i < poop.length ; i++) {
					for(j = 0 ; j < poop.length ; j++) { 
						it = poop[i]; 
					  	jt = poop[j]; 
						if(i==j) continue; 
						// not collide with self 
						r = it.r + jt.r; 
						// it.c is the centroid of each county and initial position of circle 
						itx = it.cx + 6; 
						ity = it.cy + 6; 
						jtx = jt.cx + 6; 
						jty = jt.cy + 6; 
						// distance between centroid of two circles 
						d = Math.sqrt( (itx - jtx) * (itx - jtx) + (ity - jty) * (ity - jty) ); 
						if(r > d) { 
							// there's a collision if distance is smaller than radius
							dr = ( r - d ) / ( d * 1 );
							it.cx = it.cx + ( itx - jtx ) * dr;
							it.cy = it.cy + ( ity - jty ) * dr;
						}
					}
				}
				circles.transition().ease("cubic")
			   	.attr("r", function(d, i){ 
			   		if(d.r < 3.5){
			   			d.r = 1;
			   		}else{
			   			return d.r - 2.5;
			   		}
			   	 })
				.attr("cx",function(it) { return it.cx; })
				.attr("cy",function(it) { return it.cy; });
			});

			force.start();

	}

    var tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function(d) {
      	return '<div>' + 
      	d.Team + '</div><div>' +
      	d["off"+theyear] + '</div><div>' +
      	d["def"+theyear] + '</div><div>' +
      	d["wins"+theyear] + '</div>';
      });

    var circtip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function(d) {
      	return '<div>' + d.yr + '</div><div>' + d.tval + '</div>';
      });

    svg.call(tip).call(circtip);       

    var map = svg.selectAll('.state')
      .data(data)
      .enter().append('path')
      .attr('class', 'state')
      .attr('d', path)
      .attr('fill', '#fff')
      .attr('stroke', '#000')
      .attr('stroke-width', '1');

});
});




d3.json("twCounty2010.topo.json", function (data) {
// load data with topojson.js
topo = topojson.feature(data, data.objects["twCounty2010.geo"]);
 
// prepare a Mercator projection for Taiwan
prj = d3.geo.mercator().center([120.979531, 23.978567]).scale(50000);
path = d3.geo.path().projection(prj);
 
// render them on a svg element with id "map"
blocks = d3.select("svg#map").selectAll("path").data(topo.features).enter()
.append("path").attr("d",path);
});